package urial.client.gui.dialogue;

import java.awt.*;
import java.awt.event.*;
import urial.client.gui.*;
import urial.client.run.DataSet;
import urial.core.data.*;

public final class Master extends Frame
{
	private static final long serialVersionUID = 50168866089187140L;

	private final MenuItem[] arrayItemFile = new MenuItem[] {new MenuItem("New"),new MenuItem("Load..."),new MenuItem("Save"),new MenuItem("Save as..."),new MenuItem("Quit")};
	private final MenuItem[] arrayItemEdit = new MenuItem[] {new MenuItem("Wire"),new MenuItem("Add part..."),new MenuItem("Switch hole availability"),new MenuItem("Solder"),new MenuItem("Delete")};
	private final Menu[] arrayMenu = new Menu[] {new Menu("File"),new Menu("Edit")};
	private final MenuBar bar = new MenuBar();
	private final Display display = new Display();
	private RenderingDaemon daemonRendering = new RenderingDaemon(this.display);

	public Master()
	{
		super("COOL TITLE");

		this.initField();
		this.initListener();
		this.initPane();

		super.setSize(707,500);
		super.setResizable(true);
	}

	@Override
	public void setVisible(boolean b)
	{
		super.setVisible(b);

		if(b) {
			this.daemonRendering = new RenderingDaemon(this.display);
			Thread thread = new Thread(this.daemonRendering);
			thread.start();
		}
		else 
			this.daemonRendering.stopRendering();
	}

	private void initField()
	{
		for(int i=0; i<this.arrayItemFile.length; i++) this.arrayMenu[0].add(this.arrayItemFile[i]);
		for(int i=0; i<this.arrayItemEdit.length; i++) this.arrayMenu[1].add(this.arrayItemEdit[i]);
		for(int i=0; i<this.arrayMenu.length; i++) this.bar.add(this.arrayMenu[i]);
	}

	private void initListener()
	{
		FileMIListener listenerFileMI = new FileMIListener();
		EditMIListener listenerEditMI = new EditMIListener();
		QuitListener listenerQuit = new QuitListener();

		for(int i=0; i<this.arrayItemFile.length; i++) this.arrayItemFile[i].addActionListener(listenerFileMI);
		for(int i=0; i<this.arrayItemEdit.length; i++) this.arrayItemEdit[i].addActionListener(listenerEditMI);

		super.addWindowListener(listenerQuit);
	}

	private void initPane()
	{
		super.setMenuBar(this.bar);
		super.add(this.display,BorderLayout.CENTER);
	}

	class FileMIListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			if(src.equals(Master.this.arrayItemFile[0])) {
				
			}
			if(src.equals(Master.this.arrayItemFile[1])) {
				
			}
			if(src.equals(Master.this.arrayItemFile[2])) {
				
			}
			if(src.equals(Master.this.arrayItemFile[3])) {
				
			}
			if(src.equals(Master.this.arrayItemFile[4])) {
				
			}
		}
	}

	class EditMIListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			if(src.equals(Master.this.arrayItemEdit[0])) {
				
			}
			if(src.equals(Master.this.arrayItemEdit[1])) {
				PartsChooser dialog = new PartsChooser(Master.this);
				dialog.setVisible(true);
				Coordinate c = Master.this.display.getSelectedCoordinate();
				Parts p = dialog.getResult(c.getX(),c.getY());
				DataSet.board.addParts(p);
			}
			if(src.equals(Master.this.arrayItemEdit[2])) {
				
			}
			if(src.equals(Master.this.arrayItemEdit[3])) {
				
			}
			if(src.equals(Master.this.arrayItemEdit[4])) {
				
			}
		}
	}

	class QuitListener extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			System.exit(0);
		}
	}
}
