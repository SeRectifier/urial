package urial.client.gui.dialogue;

import java.awt.*;
import java.awt.event.*;

import urial.core.data.*;
import urial.client.run.*;

public final class PartsChooser extends Dialog
{
	private static final long serialVersionUID = -8393988861065989042L;

	private final List listParts = new List();
	private final Button buttonCancel = new Button("Cancel");
	private final Button buttonAdd = new Button("Add parts");

	private boolean decisionMade = false;

	public PartsChooser(Frame owner)
	{
		super(owner,"Select parts",true);

		this.initField();
		this.initListener();
		this.initPane();

		super.setSize(423,300);
		super.setResizable(true);
	}

	public Parts getResult(int x,int y)
	{
		if(!this.decisionMade) return null;
		Box box = DataSet.library[this.listParts.getSelectedIndex()];
		return box.take(x,y);
	}

	private void initField()
	{
		for(int i=0; i<DataSet.library.length; i++) 
			this.listParts.add(DataSet.library[i].name + (DataSet.library[i].isValid() ? "" : "(Invalid)"));

		this.listParts.select(0);
	}

	private void initListener()
	{
		Listener listener = new Listener();
		this.buttonCancel.addActionListener(listener);
		this.buttonAdd.addActionListener(listener);
		super.addWindowListener(listener);
	}

	private void initPane()
	{
		Panel paneSouth = new Panel(new FlowLayout());
		paneSouth.add(this.buttonCancel);
		paneSouth.add(this.buttonAdd);

		super.add(this.listParts,BorderLayout.CENTER);
		super.add(paneSouth,BorderLayout.SOUTH);
	}

	class Listener extends WindowAdapter implements ActionListener
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			PartsChooser.this.decisionMade = false;
			PartsChooser.super.dispose();
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();
			PartsChooser.this.decisionMade = src.equals(PartsChooser.this.buttonAdd);
			PartsChooser.super.dispose();
		}
	}
}
