package urial.client.gui;

import java.awt.*;
import java.awt.event.*;

import urial.client.run.*;
import urial.core.data.*;
import urial.core.rendering.*;

public final class Display extends Canvas
{
	private static final long serialVersionUID = 8538111175649394047L;

	private int widthCanvas = 0;
	private int heightCanvas = 0;
	private int widthRender = 0;
	private int heightRender = 0;
	private int marginX = 0;
	private int marginY = 0;
	private int sizeGrid = 0;
	private int marginMin = 32;
	private float ratioCanvas = 0.0f;
	private float ratioTarget = 0.0f;
	private Rectangle areaValid = new Rectangle(0,0,0,0);

	private Color colorSelected = new Color(192,32,32,128);

	private Coordinate coordinateSelected = new Coordinate(0,0);

	private Board target = null;

	public Display()
	{
		super.addMouseListener(new EditListener());
	}

	public Coordinate getSelectedCoordinate()
	{
		return this.coordinateSelected.clone();
	}

	@Override
	public void paint(Graphics g)
	{
		this.target = DataSet.board;
		if(this.target == null) return;

		this.widthCanvas = super.getWidth();
		this.heightCanvas = super.getHeight();
		this.ratioCanvas = (float) this.heightCanvas / (float) this.widthCanvas;
		this.ratioTarget = (float) this.target.getHeight() / (float) this.target.getWidth();

		if(ratioTarget < this.ratioCanvas) {
			this.marginX = this.marginMin;
			this.widthRender = this.widthCanvas - this.marginX * 2;
			this.sizeGrid = Math.round((float) this.widthRender / (float) this.target.getWidth());
			this.heightRender = this.sizeGrid * this.target.getHeight();
			this.marginY = Math.round((float) (this.heightCanvas - this.heightRender) / 2.0f);
		}
		else {
			this.marginY = this.marginMin;
			this.heightRender = this.heightCanvas - this.marginY * 2;
			this.sizeGrid = Math.round((float) this.heightRender / (float) this.target.getHeight());
			this.widthRender = this.sizeGrid * this.target.getWidth();
			this.marginX = Math.round((float) (this.widthCanvas - this.widthRender) / 2.0f);
		}

		this.areaValid = new Rectangle(Display.this.marginX,Display.this.marginY,Display.this.widthRender,Display.this.heightRender);

		g.clearRect(0,0,this.widthCanvas,this.heightCanvas);

		for(int x=0; x<this.target.getWidth(); x++) {
		for(int y=0; y<this.target.getHeight(); y++) {
			g.drawImage(
					GraphicalResources.get(GraphicalResources.KEY_GRID_DEFAULT),
					this.marginX + this.sizeGrid * x,
					this.marginY + this.sizeGrid * y,
					this.sizeGrid,
					this.sizeGrid,
					this
			);
		}}

		this.target.display(g,this,this.sizeGrid,this.marginX,this.marginY);

		g.setColor(this.colorSelected);
		g.fillRect(
				this.marginX + this.sizeGrid * this.coordinateSelected.getX(),
				this.marginY + this.sizeGrid * this.coordinateSelected.getY(),
				this.sizeGrid,
				this.sizeGrid
		);
	}

	class EditListener implements MouseListener
	{
		@Override
		public void mouseClicked(MouseEvent e)
		{
			Point act = e.getPoint();
			if(!Display.this.areaValid.contains(act)) return;

			int xSelected = Math.round((float) (act.x - Display.this.marginX - Display.this.sizeGrid / 2) / (float) Display.this.widthRender * (float) Display.this.target.getWidth());
			int ySelected = Math.round((float) (act.y - Display.this.marginY - Display.this.sizeGrid / 2) / (float) Display.this.heightRender * (float) Display.this.target.getHeight());
			Display.this.coordinateSelected.set(xSelected,ySelected);
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
			
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}
	}
}
