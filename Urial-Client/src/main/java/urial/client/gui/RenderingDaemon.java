package urial.client.gui;

import java.awt.*;
import java.awt.image.*;

public final class RenderingDaemon implements Runnable
{
	private Canvas target = null;
	private long timeWait = 33L;
	private boolean isActive = false;

	public RenderingDaemon(Canvas c)
	{
		if(c == null) throw new IllegalArgumentException("Null canvas");
		this.target = c;
		this.target.setIgnoreRepaint(true);
	}

	@Override
	public void run()
	{
		this.target.createBufferStrategy(2);
		BufferStrategy strategy = this.target.getBufferStrategy();
		Graphics g = null;
		this.isActive = true;

		while(this.isActive) {
			if(strategy.contentsLost()) continue;
			g = strategy.getDrawGraphics();
			this.target.paint(g);
			try {
				Thread.sleep(this.timeWait);
			}
			catch(InterruptedException ie) {}
			g.dispose();
			strategy.show();
		}

		strategy.dispose();
	}

	public void stopRendering()
	{
		this.isActive = false;
	}
}
