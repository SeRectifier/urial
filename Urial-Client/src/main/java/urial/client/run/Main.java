package urial.client.run;

import java.io.*;
import java.util.*;

import urial.client.gui.dialogue.*;
import urial.core.data.*;
import urial.core.io.*;
import urial.core.rendering.*;

public final class Main
{
	public static void main(String[] args)
	{
		try {
			File ad = new File(new File(System.getProperty("user.dir")),"assets");
			GraphicalResources.initialize(ad);

			File ld = new File(new File(System.getProperty("user.dir")),"libraries");
			File[] arrayFile = ld.listFiles();
			ArrayList<Box> listBox = new ArrayList<Box>(0);
			Box box = null;
			for(int i=0; i<arrayFile.length; i++) {
				if(arrayFile[i].isDirectory()) continue;
				if(!arrayFile[i].getName().endsWith(".txt")) continue;
				box = LibraryParser.parse(arrayFile[i]);
				if(box.isValid()) listBox.add(box);
			}

			Box[] arrayBox = new Box[listBox.size()];
			for(int i=0; i<listBox.size(); i++) arrayBox[i] = listBox.get(i);
			DataSet.library = arrayBox;
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
			System.exit(1);
		}

		new Master().setVisible(true);
	}
}
