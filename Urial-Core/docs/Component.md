# urial.common.data.Component

Abstract

### Constructors
- protected Component(String name)

### Methods
###### Setter
- public void setName(String n)
- public void setDescription(String d)

###### Getter
- public String getName()
- public String getDescription()

###### Other