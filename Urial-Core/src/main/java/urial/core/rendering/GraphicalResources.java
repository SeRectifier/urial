package urial.core.rendering;

import java.awt.image.*;
import java.io.*;
import java.util.*;
import javax.imageio.*;

public final class GraphicalResources
{
	public static final String KEY_DEPRECATED = "deprecated";
	public static final String KEY_GRID_DEFAULT = "grid_default";
	public static final String KEY_HOLE_SOLDERED = "hole_soldered";
	public static final String KEY_HOLE_DISABLED = "hole_disabled";
	public static final String KEY_HOLE_COVERED = "hole_covered";
	public static final String KEY_HOLE_Q = "hole_q";
	public static final String KEY_WIRE_NORTH = "wire_north";
	public static final String KEY_WIRE_SOUTH = "wire_south";
	public static final String KEY_WIRE_EAST = "wire_east";
	public static final String KEY_WIRE_WEST = "wire_west";

	private static HashMap<String,BufferedImage> mapGeneralImage = null;

	public static void initialize(File dir) throws IOException
	{
		String extension = ".png";
		String[] arrayImageFileName = new String[] {
				GraphicalResources.KEY_DEPRECATED,
				GraphicalResources.KEY_GRID_DEFAULT,
				GraphicalResources.KEY_HOLE_SOLDERED,
				GraphicalResources.KEY_HOLE_DISABLED,
				GraphicalResources.KEY_HOLE_COVERED,
				GraphicalResources.KEY_HOLE_Q,
				GraphicalResources.KEY_WIRE_NORTH,
				GraphicalResources.KEY_WIRE_SOUTH,
				GraphicalResources.KEY_WIRE_EAST,
				GraphicalResources.KEY_WIRE_WEST,
		};

		GraphicalResources.mapGeneralImage = new HashMap<String,BufferedImage>(0);
		for(int i=0; i<arrayImageFileName.length; i++) 
			GraphicalResources.mapGeneralImage.put(arrayImageFileName[i],ImageIO.read(new File(dir,arrayImageFileName[i] + extension)));
	}

	public static BufferedImage get(String key)
	{
		return GraphicalResources.mapGeneralImage.get(key);
	}
}
