package urial.core.rendering;

import java.awt.*;
import java.awt.image.*;

public interface Displayable
{
	public void display(Graphics g,ImageObserver obsvr,int size,int ox,int oy);
}
