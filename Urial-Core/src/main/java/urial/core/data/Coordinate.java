package urial.core.data;

import java.io.*;

public class Coordinate implements Serializable,Cloneable
{
	private static final long serialVersionUID = 7508886239829275774L;

	protected int x = 0;
	protected int y = 0;

	public Coordinate() {}

	public Coordinate(int x,int y)
	{
		this.setX(x);
		this.setY(y);
	}

	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Coordinate == false) return false;
		Coordinate another = (Coordinate) obj;

		return (another.getX() == this.x && another.getY() == this.y);
	}

	@Override
	public Coordinate clone()
	{
		return new Coordinate(this.x,this.y);
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public void set(int x,int y)
	{
		this.x = x;
		this.y = y;
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}
}
