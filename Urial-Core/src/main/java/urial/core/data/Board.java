package urial.core.data;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

import urial.core.rendering.*;

public class Board extends Component implements Serializable, Displayable
{
	private static final long serialVersionUID = -1825024220718321734L;

	// MSB [Q][Covered][Disabled][Soldered][N][S][E][W] LSB
	protected byte[][] grid = new byte[super.width][super.height];
	protected ArrayList<Parts> listParts = new ArrayList<Parts>(0);

	public Board(String name,int w,int h)
	{
		super(name,0,0,w,h);
	}

	public void addParts(Parts p)
	{
		if(p == null) return;
		Coordinate[] arrayPC = p.getWherePinsAre();
		for(int i=0; i<arrayPC.length; i++) this.setSoldered(arrayPC[i].getX(),arrayPC[i].getY(),true);
		this.listParts.add(p);
	}

	public void removePart(Parts p)
	{
		if(p == null) return;
		this.listParts.remove(p);
	}

	public void removePart(int index)
	{
		this.listParts.remove(index);
	}

	public void wireTo(Direction d,int x,int y)
	{
		switch(d) {
		case NORTH:
			this.grid[x][y] = (byte) (this.grid[x][y] | 0b00000001);
			break;
		case SOUTH:
			this.grid[x][y] = (byte) (this.grid[x][y] | 0b00000010);
			break;
		case EAST:
			this.grid[x][y] = (byte) (this.grid[x][y] | 0b00000100);
			break;
		case WEST:
			this.grid[x][y] = (byte) (this.grid[x][y] | 0b00001000);
			break;
		default:
			break;
		}
	}

	public void unwireTo(Direction d,int x,int y)
	{
		switch(d) {
		case NORTH:
			this.grid[x][y] = (byte) (this.grid[x][y] & 0b11111110);
			break;
		case SOUTH:
			this.grid[x][y] = (byte) (this.grid[x][y] & 0b11111101);
			break;
		case EAST:
			this.grid[x][y] = (byte) (this.grid[x][y] & 0b11111011);
			break;
		case WEST:
			this.grid[x][y] = (byte) (this.grid[x][y] & 0b11110111);
			break;
		default:
			break;
		}
	}

	public void setSoldered(int x,int y,boolean b)
	{
		if(b) this.grid[x][y] = (byte) (this.grid[x][y] | 0b00010000);
		else this.grid[x][y] = (byte) (this.grid[x][y] & 0b11101111);
	}

	public void setEnabled(int x,int y,boolean b)
	{
		if(b) this.grid[x][y] = (byte) (this.grid[x][y] | 0b00100000);
		else this.grid[x][y] = (byte) (this.grid[x][y] & 0b11011111);
	}

	public void setCovered(int x,int y,boolean b)
	{
		if(b) this.grid[x][y] = (byte) (this.grid[x][y] | 0b01000000);
		else this.grid[x][y] = (byte) (this.grid[x][y] & 0b10111111);
	}

	public boolean isWiredTo(Direction d,int x,int y)
	{
		switch(d) {
		case NORTH:
			return (((this.grid[x][y] >> 0) & 0b00000001) == 1);
		case SOUTH:
			return (((this.grid[x][y] >> 1) & 0b00000001) == 1);
		case EAST:
			return (((this.grid[x][y] >> 2) & 0b00000001) == 1);
		case WEST:
			return (((this.grid[x][y] >> 3) & 0b00000001) == 1);
		default :
			return false;	
		}
	}

	public boolean isSolderedHole(int x,int y)
	{
		return ((this.grid[x][y] >> 4) & 0b00000001) == 1;
	}

	public boolean isDisabledHole(int x,int y)
	{
		return ((this.grid[x][y] >> 5) & 0b00000001) == 1;
	}

	public boolean isCovered(int x,int y)
	{
		return ((this.grid[x][y] >> 6) & 0b00000001) == 1;
	}

	@Deprecated
	public boolean isQ(int x,int y)
	{
		return ((this.grid[x][y] >> 7) & 0b00000001) == 1;
	}

	@Override
	public void display(Graphics g, ImageObserver obsvr, int size, int ox, int oy)
	{
		for(int i=0; i<this.listParts.size(); i++) 
			this.listParts.get(i).display(g,obsvr,size,ox,oy);

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			if(this.isWiredTo(Direction.NORTH,x,y)) {
				g.drawImage(
						GraphicalResources.get(GraphicalResources.KEY_WIRE_NORTH),
						ox + size * x,
						oy + size * y,
						size,
						size,
						obsvr
				);
			}
			if(this.isWiredTo(Direction.SOUTH,x,y)) {
				g.drawImage(
						GraphicalResources.get(GraphicalResources.KEY_WIRE_SOUTH),
						ox + size * x,
						oy + size * y,
						size,
						size,
						obsvr
				);
			}
			if(this.isWiredTo(Direction.EAST,x,y)) {
				g.drawImage(
						GraphicalResources.get(GraphicalResources.KEY_WIRE_EAST),
						ox + size * x,
						oy + size * y,
						size,
						size,
						obsvr
				);
			}
			if(this.isWiredTo(Direction.WEST,x,y)) {
				g.drawImage(
						GraphicalResources.get(GraphicalResources.KEY_WIRE_WEST),
						ox + size * x,
						oy + size * y,
						size,
						size,
						obsvr
				);
			}
			if(this.isSolderedHole(x,y)) {
				g.drawImage(
						GraphicalResources.get(GraphicalResources.KEY_HOLE_SOLDERED),
						ox + size * x,
						oy + size * y,
						size,
						size,
						obsvr
				);
			}
			if(this.isDisabledHole(x,y)) {
				g.drawImage(
						GraphicalResources.get(GraphicalResources.KEY_HOLE_DISABLED),
						ox + size * x,
						oy + size * y,
						size,
						size,
						obsvr
				);
			}
		}}
	}

	public enum Direction
	{
		NORTH,
		SOUTH,
		EAST,
		WEST,
	}
}
