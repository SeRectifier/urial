package urial.core.data;

import java.awt.*;
import java.awt.image.*;

import urial.core.rendering.*;

public class Parts extends Component implements Displayable
{
	private static final long serialVersionUID = -4600749088531631128L;

	protected String number = null;
	protected String value = null;
	protected Pin[] arrayPins = null;
	protected Symbol symbol = null;

	public Parts(String name,String d,int x,int y,int w,int h,String number,String value,Pin[] arrayPins,Symbol symbol)
	{
		super(name,x,y,w,h);
		super.setDescription(d);

		this.number = number;
		this.value = value;
		this.arrayPins = arrayPins;
		this.symbol = symbol;

		if(this.symbol == null) throw new IllegalArgumentException("Null symbol");
		if(this.arrayPins == null) throw new IllegalArgumentException("Null pin array");
		if(this.arrayPins.length == 0) throw new IllegalArgumentException("Invalid pin array");

		Pin pinCurrent = null;
		for(int i=0; i<this.arrayPins.length; i++) {
			pinCurrent = this.arrayPins[i];
			for(int j=i+1; j<this.arrayPins.length; j++) {
				if(pinCurrent.getCoordinate().equals(this.arrayPins[j].getCoordinate())) 
					throw new IllegalArgumentException("Conflicting pin coordinate");
				if(pinCurrent.getNumber() == this.arrayPins[j].getNumber())
					throw new IllegalArgumentException("Conflicting pin number");
			}
		}
	}

	public void setNumber(String num)
	{
		this.number = num;
	}

	public void setValue(String v)
	{
		this.value = v;
	}

	public void setSymbol(Symbol s)
	{
		if(s == null) return;
		this.symbol = s;
	}

	public String getNumber()
	{
		return this.number;
	}

	public String getValue()
	{
		return this.value;
	}

	public Symbol getSymbol()
	{
		return this.symbol.clone();
	}

	// This method returns absolute coordinate
	public Coordinate[] getWherePinsAre()
	{
		Coordinate[] arrayCoordinate = new Coordinate[this.arrayPins.length];
		for(int i=0; i<arrayCoordinate.length; i++)
			arrayCoordinate[i] = new Coordinate(this.coordinateX + this.arrayPins[i].getX(),this.coordinateY + this.arrayPins[i].getY());

		return arrayCoordinate;
	}

	@Override
	public void display(Graphics g, ImageObserver obsvr,int size, int ox, int oy)
	{
		g.drawImage(
				this.symbol.toImage(),
				ox + this.coordinateX * size,
				oy + this.coordinateY * size,
				this.width * size,
				this.height * size,
				obsvr
		);
	}
}
