package urial.core.data;

import java.io.*;

public abstract class Component implements Serializable
{
	private static final long serialVersionUID = 8488991884233904056L;

	public static final String VALUE_UNNAMED = "unnamed";
	public static final String VALUE_UNSPECIFIED = "unspecified";

	protected int coordinateX = 0;
	protected int coordinateY = 0;
	protected int width = 1;
	protected int height = 1;
	protected String textName = Component.VALUE_UNNAMED;
	protected String textDescription = null;

	protected Component(String name,int x,int y,int w,int h)
	{
		this.setName(name);
		this.setX(x);
		this.setY(y);
		this.setWidth(w);
		this.setHeight(h);
	}

	public void setX(int x)
	{
		this.coordinateX = x;
	}

	public void setY(int y)
	{
		this.coordinateY = y;
	}

	public void setWidth(int w)
	{
		if(w < 1) throw new IllegalArgumentException("Invalid width");
		this.width = w;
	}

	public void setHeight(int h)
	{
		if(h < 1) throw new IllegalArgumentException("Invalid height");
		this.height = h;
	}

	public void setName(String n)
	{
		if(n == null) this.textName = Component.VALUE_UNNAMED;
		else if(n.trim().length() == 0) this.textName = Component.VALUE_UNNAMED;
		else this.textName = n;
	}

	public void setDescription(String d)
	{
		this.textDescription = d;
	}

	public int getX()
	{
		return this.coordinateX;
	}

	public int getY()
	{
		return this.coordinateY;
	}

	public int getWidth()
	{
		return this.width;
	}

	public int getHeight()
	{
		return this.height;
	}

	public String getName()
	{
		return this.textName;
	}

	public String getDescription()
	{
		return this.textDescription;
	}
}
