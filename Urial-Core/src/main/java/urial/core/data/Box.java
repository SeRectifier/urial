package urial.core.data;

import java.io.*;

public class Box implements Serializable
{
	private static final long serialVersionUID = -4821924992543681667L;

	public int width = 0;
	public int height = 0;
	public String name = null;
	public String d = null;
	public Pin[] arrayPins = null;
	public Symbol symbol = null;

	public Box() {}

	public Box(int w,int h,Pin[] pins, Symbol symbol)
	{
		this.width = w;
		this.height = h;
		this.arrayPins = pins;
		this.symbol = symbol;
	}

	public boolean isValid()
	{
		if(this.width < 1 || this.height < 1) return false;
		if(this.name == null) return false;
		if(this.name.trim().length() == 0) return false;
		if(this.symbol == null) return false;
		if(this.arrayPins == null) return false;
		if(this.arrayPins.length == 0) return false;

		Pin pinCurrent = null;
		for(int i=0; i<this.arrayPins.length; i++) {
			pinCurrent = this.arrayPins[i];
			for(int j=i+1; j<this.arrayPins.length; j++) {
				if(pinCurrent.getCoordinate().equals(this.arrayPins[j].getCoordinate())) return false;
				if(pinCurrent.getNumber() == this.arrayPins[j].getNumber()) return false;
			}
		}

		return true;
	}

	public Parts take(int x,int y)
	{
		if(this.isValid() == false) return null;

		Parts part = new Parts(
				this.name,
				d,
				x,
				y,
				this.width,
				this.height,
				Component.VALUE_UNSPECIFIED,
				Component.VALUE_UNSPECIFIED,
				this.arrayPins,
				this.symbol
		);

		return part;
	}
}
