package urial.core.data;

import java.awt.image.*;
import java.io.*;

public final class Symbol implements Serializable,Cloneable
{
	private static final long serialVersionUID = 4715617065478219785L;

	private int width = 0;
	private int height = 0;
	private int[][] grid = new int[this.width][this.height];

	public Symbol(int w,int h)
	{
		if(w < 1 || h < 1) throw new IllegalArgumentException("Invalid size");
		this.initialize(w,h);
	}

	public Symbol(BufferedImage img)
	{
		if(img == null) throw new IllegalArgumentException("Null image");

		this.width = img.getWidth();
		this.height = img.getHeight();
		this.grid = new int[this.width][this.height];
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = img.getRGB(x,y);
		}}
	}

	@Override
	public Symbol clone()
	{
		Symbol c = new Symbol(this.width,this.height);
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			c.setColorAt(x,y,this.grid[x][y]);
		}}

		return c;
	}

	public void initialize()
	{
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = 0;
		}}
	}

	public void initialize(int w,int h)
	{
		if(w < 1 || h < 1) return;
		this.width = w;
		this.height = h;

		this.grid = new int[this.width][this.height];
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = 0;
		}}
	}

	public void setWidth(int w)
	{
		if(w < 1) return;

		int[][] gridOld = this.grid;
		int widthOld = this.width;
		this.width = w;
		this.grid = new int[this.width][this.height];

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = x < widthOld ? gridOld[x][y] : 0;
		}}
	}

	public void setHeight(int h)
	{
		if(h < 1) return;

		int[][] gridOld = this.grid;
		int heightOld = this.height;
		this.height = h;
		this.grid = new int[this.width][this.height];

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = y < heightOld ? gridOld[x][y] : 0;
		}}
	}

	public void setSize(int w,int h)
	{
		if(w < 1 || h < 1) return;

		int[][] gridOld = this.grid;
		int widthOld = this.width;
		int heightOld = this.height;
		this.grid = new int[this.width][this.height];

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = (x < widthOld && y < heightOld) ? gridOld[x][y] : 0;
		}}
	}

	public void setColorAt(int x,int y,int argb)
	{
		this.grid[x][y] = argb;
	}

	public BufferedImage toImage()
	{
		BufferedImage img = new BufferedImage(this.width,this.height,BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			img.setRGB(x,y,this.grid[x][y]);
		}}

		return img;
	}

	public int getWidth()
	{
		return this.width;
	}

	public int getHeight()
	{
		return this.height;
	}
}
