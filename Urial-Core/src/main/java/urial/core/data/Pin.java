package urial.core.data;

import java.io.*;

public class Pin implements Serializable
{
	private static final long serialVersionUID = -1087392358161777205L;

	protected int number = 1;
	protected int coordinateX = 0;
	protected int coordinateY = 0;
	protected String textName = Component.VALUE_UNNAMED;
	protected String textDescription = null;

	public Pin(int n,int x,int y,String name,String d)
	{
		if(n < 1) throw new IllegalArgumentException("Invalid pin number");
		if(x < 0 || y < 0) throw new IllegalArgumentException("Negative coordinate");

		this.number = n;
		this.coordinateX = x;
		this.coordinateY = y;
		this.setName(name);
		this.setDescription(d);
	}

	public void setNumber(int n)
	{
		if(n < 1) return;
		this.number = n;
	}

	public void setX(int x)
	{
		if(x < 0) this.coordinateX = 0;
		else this.coordinateX = x;
	}

	public void setY(int y)
	{
		if(y < 0) this.coordinateY = 0;
		else this.coordinateY = y;
	}

	public void setName(String name)
	{
		this.textName = name == null ? Component.VALUE_UNNAMED : name;
	}

	public void setDescription(String d)
	{
		this.textDescription = d;
	}

	public int getNumber()
	{
		return this.number;
	}

	public int getX()
	{
		return this.coordinateX;
	}

	public int getY()
	{
		return this.coordinateY;
	}

	public Coordinate getCoordinate()
	{
		return new Coordinate(this.coordinateX,this.coordinateY);
	}

	public String getName()
	{
		return this.textName;
	}

	public String getDescription()
	{
		return this.textDescription;
	}
}
