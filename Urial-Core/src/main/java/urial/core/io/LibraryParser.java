package urial.core.io;

import java.io.*;
import java.util.*;
import javax.imageio.*;
import urial.core.data.*;

public final class LibraryParser
{
	public static Box parse(File file) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayDeque<String> queueSyntax = new ArrayDeque<String>(0);
		String cache = reader.readLine();
		while(cache != null) {
			queueSyntax.push(cache.trim());
			cache = reader.readLine();
		}
		reader.close();

		Box box = new Box();
		ArrayList<Pin> listPins = new ArrayList<Pin>(0);
		String line = queueSyntax.pollLast();
		while(line != null) {
			try {
				if(line.startsWith("width:")) 
					box.width = Integer.parseInt(line.substring(6));
				if(line.startsWith("height:")) 
					box.height = Integer.parseInt(line.substring(7));
				if(line.startsWith("name:")) 
					box.name = line.substring(5);
				if(line.startsWith("symbol:")) {
					String sfn = line.substring(7);
					File target = new File(file.getParent(),sfn);
					try {
						box.symbol = new Symbol(ImageIO.read(target));
					}
					catch(IOException ioe) {}
				}
				if(line.equals("description:")) {
					line = queueSyntax.pollLast();
					StringBuffer buffer = new StringBuffer();
					while(line != null) {
						if(line.equals(":enddescription")) break;
						buffer.append(line);
						line = queueSyntax.pollLast();
					}
					box.d = buffer.toString();
				}
				if(line.equals("pin:")) {
					RawPin rp = new RawPin();
					line = queueSyntax.pollLast();
					while(line != null) {
						if(line.equals(":endpin")) break;
						try {
							if(line.startsWith("number:")) 
								rp.number = Integer.parseInt(line.substring(7));
							if(line.startsWith("x:")) 
								rp.x = Integer.parseInt(line.substring(2));
							if(line.startsWith("y:")) 
								rp.y = Integer.parseInt(line.substring(2));
							if(line.startsWith("name:")) 
								rp.name = line.substring(5);
							if(line.equals("description:")) {
								StringBuffer buffer = new StringBuffer();
								line = queueSyntax.pollLast();
								while(line != null) {
									if(line.equals(":enddescription")) break;
									buffer.append(line + System.lineSeparator());
									line = queueSyntax.pollLast();
								}
								rp.d = buffer.toString();
							}
						}
						catch(NumberFormatException nfe) {}
						finally {
							line = queueSyntax.pollLast();
						}
					}
					if(rp.isValid()) listPins.add(new Pin(rp.number,rp.x,rp.y,rp.name,rp.d));
				}
			}
			catch(NumberFormatException nfe) {}
			finally {
				line = queueSyntax.pollLast();
			}
		}

		Pin[] arrayPins = new Pin[listPins.size()];
		for(int i=0; i<listPins.size(); i++) arrayPins[i] = listPins.get(i);
		box.arrayPins = arrayPins;

		return box;
	}

	static class RawPin
	{
		public int number = 0;
		public int x = -1;
		public int y = -1;
		public String name = null;
		public String d = null;

		public boolean isValid()
		{
			if(this.number < 1) return false;
			if(this.x < 0 || this.y < 0) return false;
			if(this.name == null) return false;
			if(this.name.trim().length() == 0) return false;

			return true;
		}
	}
}
